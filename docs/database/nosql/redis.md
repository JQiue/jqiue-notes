---
title: Redis
category: 数据库
tags: [Alpha]
author: JQiue
article: false
---

## 介绍

## 安装

最新的软件版本为 6，官方不推荐在 Windows 上使用 Redis，尽管微软维护了一份 3.2.100 版本的，但是好在 Github 社区无所不能，有人正在维护 5.0 版本的 Redis 可在 Windows 上安装，地址：[Github](https://github.com/tporadowski/redis)，这足够学习使用了
