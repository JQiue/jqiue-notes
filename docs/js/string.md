---
title: 字符串
category: Web
tags: [Alpha]
author: JQiue
article: false
---


## 方法

+ `str.chatAt(index)`：返回在指定位置的字符
+ `str.concat(str1, str2, ...)`：连接字符串
+ `str.indexOf(str)`：返回指定值第一次出现的索引
+ `lastIndexOf(str)`：返回指定值最后一次出现的索引
+ `str.match(reg)`：返回一个字符串匹配正则表达式的结果
+ `str.replace(reg|substr, newSubStr)`：返回一个由替换值（replacement）替换部分或所有的模式（pattern）匹配项后的新字符串
+ `str.search(reg)`：执行正则表达式和 String 对象之间的一个搜索匹配
+ `str.slice(start, end)`：提取某个字符串的一部分，并返回一个新的字符串，且不会改动原字符串
+ `str.split(separator)`：使用指定的分割符，将字符串分割成数组
+ `str.repeat(conut)`：返回一个新字符串，该字符串包含被连接在一起的指定数量的字符串的副本
+ `substring(start， end)`：返回一个字符串在开始索引到结束索引之间的一个子集
+ `str.trim()`：删除字符串两端的空白
+ `str.toLowerCase()`：转换为小写
+ `str.toUpperCase()`：转换为大写
+ `str.toString()`：返回指定对象的字符串形式
+ `str.valueOf()`：字符串对象转换为其对应的基本字符串

## 模板字符串

## 总结
