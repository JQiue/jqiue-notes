---
blog: true
home: true
title: true
heroFullScreen: true
heroText: 嗨，欢迎来到这里！
bgImage:  https://qn.jinqiu.wang/bgImage.jpg
bgImageStyle: Record<opacity = 0>
tagline: Welcome to here
actionText: 了解更多 →
actionLink: /about/
project:
  - name: 计算机原理
    type: article
    link: /theory/
  - name: 编程语言
    type: article
    link: /language/
  - name: Web 前端
    type: article
    link: /web/
  - name: 资源和工具
    type: article
    link: /sundry/tools/
footer: '<a href="https://beian.miit.gov.cn/" target="_blank">备案号：鄂ICP备2021016538号</a>'
---
