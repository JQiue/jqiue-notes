---
title: 一些积累
category: 学科
tags: [Alpha]
author: JQiue
article: false
---

## 过去式不规则变化

```plain
buy -> bought
lose -> lost
see -> saw
meet -> met
get -> got
```

## 过去分词不规则变化

```plain
be -> been
see -> seen
```

## 名词复数不规则变化

```plain
man -> men
woman -> women
child -> children
foot -> feet
tooth -> teeth
mouse -> mice
goose -> geese
```

## 介词短语

很多介词短语是由介词 + 名词构成的

```plain
put on 穿上
take off 脱掉
turn on 开
turn off 关
out of 自...离开
in front of 在...前面
do with 处理
next door 隔壁
a minute 一会儿
at the moment 此刻
get up 起床
hundreds of ... 数以百计的...
on the way 在...途中
take out 取出
a lot of 许多
all the time 一直，始终
```

## 动词短语

动词短语通常是指动词后面跟着一个介词或副词短语的动词，英语中存在用动词短语代替与其同义的单个动词的强烈趋势，比如听到敲门声，会说 Come in，而不是 Enter。这些动词常与表示位置或方向的词组合，比如 along，down，in，off，on，out，over，under 等

```
The cats are running along the wall.
The children are jumping off the branch.
He is going into a shop.
```

## 辨析

### large 与 big

large 仅指物理上的大，是 small 的反义词，通常指体积、面积、形状、数量方面的大，修饰人时指个子大

```plain
China is a large country.
Look at that large woman in white.
```

big 所表示的大不仅指体积大，还很重，在修饰人时，通常指大人物，但个子未必高大

```plain
It is a big house.
She's very big in the filmdom.
```

### small 与 little

small 指物理上的小或少，是 large 的反义词

```plain
It is small factory.
```

little 也表示小和少，但有小而可爱的感情色，是 big 的反义词

```plain
There is a little garden behind our house.
She has the sweetest little smile.
```

### desk 与 table

desk 通常是指有抽屉的桌子，用于办公、读书、写字等

```plain
He is working at his desk.
```

table 通常指若干条腿支撑着的评判，没有抽屉，比如“餐桌”、“会议桌”、“工作台”等

```plain
I've booked a table for two at 7.00.
```

### too 和 either

too 和 either 都表示“也”，但是 too 仅用于肯定句中，而 either 用于否定句。一般都放在句末，且用逗号隔开

### Here you are，Here it is，Here there are

是给对方东西的习惯用语，Here it is 用于单数的物，Here there are 用于复数的物

### some 和 any

some 和 any 是最常用的数量词，some 用于肯定句，或期望是肯定回答的疑问句

```
There is some water in the glass.
Have you got some paper-clips in that box?
```

any 用于否定句，或不能确定是肯定还是否定以及期望是否定回答的疑问句

```
There are not any spoons in the cupboard.
Are there any cigarettes on the floor?
```
