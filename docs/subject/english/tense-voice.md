---
title: 时态和语态
category: 学科
tags: [Alpha]
author: JQiue
article: false
---

动词的用法非常丰富，可以体现主语的单复数，也可以体现主语动作的时间和状态

+ 动词有数量和时态上的变化，时态通常由三大时态：过去，现在和将来
+ 根据动作进行的状态：一般，进行和完成
+ 使用动词时一般将前两者结合，比如：一般现在时，过去进行时

```
He goes to shchool every day.
He went to hospital last night.
```

一般过去时：表示过去的某一个时间里发生的习惯性或经常性的动作和状态，基本结构：`主语 + 过去分词 + 其他`

变化规律：

+ 一般加`ed`
+ 以字母`e`结尾的只加`d`
+ 以“辅音字母 + y” 结尾的，变 y 为 i 再加`ed`
+ 重读闭音节结尾且末尾只有一个辅音字母的双写辅音字母再加`ed`
+ 不规则变化，需要记忆

```
He worked very hard last night. 他昨天晚上工作的非常努力
They came here by car. 他们开车来到了这里
```

过去进行时：在过去的某一时刻或某一段时间内持续进行或发生的动作，基本结构：`was/were + doing`

```
They were waiting for you. 他们在等你
He was talking with his friends just now. 就在刚才他和他的朋友正在谈话
```

将来时：表示将来某一个时刻的动作和状态，或某一段时间发生的动作和状态，常与表示将来的时间状语连用：tomorrow，next week，in the future，

帮助构成将来时：

+ will 和 shall 这两个助动词可以构成将来时态，基本结构：`will/shall + 动词原形`

```
They will go to Shanghai by ship tomorrow.
We shall leave for Shanghai next month.
```

+ “be going to + 动词原形”用来表示近期或事先考虑过的将要发生的动作，意为：“打算、将要”
  
```
They are going to play football this afternoon.
She is going to leave French next year.
```

+ “be + doing”表示位置转移的动词，比如：go，come，leave，start，arrive，可用现在进行时表将来

```
They are leaving for Japan.
She is arriving tomorrow.
```

现在完成时：动作已经发生，已经完成，对现在造成影响或后果，有可能还会持续，基本结构：`have/has + 过去分词`，通常和一些时间状语连用：already（已经），yet（还）

```
They have already arrived in Shanghai. 他们已经到了上海
She has played soccer for 3 hours. 他已经踢了三个小时的足球（已经发生，还在持续）
She hasn't finished the homework yet. 她还没有完成家庭作业
```

过去完成时：动作发生在过去，已经完成，且对过去造成了一定的影响，基本结构：`had + 过去分词`

```
They had already arrived in Shanghai. 他们已经到了上海
She had played soccer for 3 hours. 她已经踢了三个小时的足球
She hadn't finished the homework yet.  她还没有完成家庭作业
```

## 时态

因为动词可以表达动作的时间以及状态，所以时态理论上有 16 种可能的时态

### 一般过去时态

用于描述过去发生的事，跟现在没有关系，比如`He was a student.`表示过去是一个学生，现在不是了

be 动词的过去式中，第一人称和第三人称单数用`was`，其他情况用`were`

+ 动词变化：过去式（did/was/were）

::: danger
动词过去式和过去分词的用法是有区别的，过去式可以构成过去时态，过去分词可以构成完成时态，还能构成被动语态以及非谓语动词
:::

### 一般现在时态

一般现在时态主要表示经常性习惯性的动作以及现在的状态，经常与副词连用，比如 every day、week、month、year、in the morning、afternoon、evening、at noon、at night、usually、always、often、sometimes、never

```
He often gets up late. 经常性
I am a student. 现在的状态
```

除此之外还可以表示“永恒”：客观事实、科学真理、名言警句

```
The earth is round.
The earth moves around the sun.
Knowledge is power.
```

动词形式有两种，要么就是原形，要么就是第三人称单数，但是 be 动词和情态动词是没有第三人称单数变化的，只有实义动词有，主语是第三人称单数就要用三单形式

+ 动词变化：原型/三单（do/does）

```
They often get up at 7:00. 他们经常在 7:00 起床（习惯性动作）
He often gets up at 7:00. 他经常在 7:00 起床（习惯性动作）
```

一般现在时的否定和疑问句用 do、does 帮助构成

::: tip 第三人称单数
代词`he,she,it`，可数名词单数和不可数名词都属于第三人称单数主语

> 有点像名词的复数变化，要做一些词性上的区分

第三人称变化规律：

+ 动词结尾加`s`
+ 以字母 s、x、ch 或 o 结尾的加`es`
+ 以辅音字母结尾的变 y 为 i 在加`es`
:::

### 一般将来时态

表示现在的将来，即将做某事。will 通常表示临时决定，be going to 表示计划好的

动词变化：

+ will + 动词原形
+ be going to + 动词原型

```
I will make a new plan tomorrow.
I will buy a car in 2020.
We are going to study abroad next year.
```

对于 be going to 的疑问式和否定式

+ 疑问：将 be 提到句首
+ 否定：在 be 后面加 not

### 过去将来时态

+ would + 动词原形
+ was/were going to + 动词原形

表示过去的将来

```
Tony finished his work. and then he would leave for London.
```

过去将来时常常和一般过去时搭配使用

### 现在进行时态

现在进行时表示现在进行或发生的动作，一般与 now，at the moment，today，this afternoon，this evening，tonight 等连用

动词形式：

+ be + doing
+ doing

```
They are watching TV. 他们正在看电视
He is watching TV. 他正在看电视
I am watching TV. 我正在看电视
```

变化规律：

+ 一般在动词后加`ing`
+ 以不发音的`e`结尾，要去掉`e`再加`ing`
+ 重读闭音节的动词，要双写词尾字母再加`ing`
+ 以`ie`结尾的，变`y`为`i`再加`ing`

将现在进行时的句子变为一般疑问句时只讲助动词提前

```
They are watching TV. => Are they watching TV?
He is watching TV. => Is he watching TV?
```

否定时要将 not 放到助动词后：

```
The dog is drinking its milk.
The dog is not drinking its milk.
```

### 过去进行时态

过去某个时间点正在发生的动作

过去一段时间持续发生的动作

动词形式：was/were + 现在分词

A young man and a young woman were sitting behind me.

### 将来进行时

将来某个时间点正在发生的动作

将来某个时间点会持续发生的动作

动词形式：will be doing

### 现在完成时

过去的动作对现在造成的影响

动词形式：have/has + 过去分词

标志词：for，since，already，yet，before，over the ...，

I have learned English for 10 years ago.

I had learned English for 10 years when I graduated.

I will have learned English for 10 years when I graduate.

### 过去完成时

过去的过去的动作对过去的某个时间点造成的影响

动词形式：had + 过去分词

I have seen the movie

I had seen the movie when she invited me.

I will have seen the seen movie when she arrived in Beijing.

### 将来完成时

将来的动作对将来的某个时间点造成的影响

动词形式：will + have + 过去分词

### 总结

时间/状态 | 现在 | 过去 | 将来 | 过去将来
---|---|---|---|---
一般 | am/is/are，do | was/were，did | shall/will do | would do
进行 | am/is/are doing | was/were doing | shall/will be doing | would be doing
完成 | have/has done | had done | will have done | would have done
完成进行 | have/has been doing | had been doing | will have been doing | would have been doing

::: tip have

+ 实意动词，表‘拥有’
+ have + 过去分词，构成完成时态
+ have + been + 现在分词，构成完成进行时态
+ have + been + 过去分词，构成完成时被动语态
+ have + n. 表示其他含义
+ have to...，客观上不得不做某事
:::

## 语态

一般情况下，都是主语发生的动作，这叫做主动语态，只要句子不是被动的，那么就是主动的，只需要学习被动语态即可

被动语态就是表示客观事实，比如`我吃了那个蛋糕`就是主观事实，如果想表示“谁吃了那个蛋糕”，就无法用主观来表达了，但是从意思上来讲已经知道“蛋糕被吃了”，但不知道谁是动作的发出者，这就是客观事实，只要将宾语提前就变成了“蛋糕被狗吃了”，但是被动语态不一定有表面上的“被”，比如“教师每天都打扫”，这说明被动语态跟“被”字是没关系的，得根据句子中的意思来判断

+ 及物动词
+ 不及物动词 + 介词

形式：be + done

done 表示被动的动作，be 表示被动的时间（变成对应的各种时态），不仅如此 be 还要表示单复数
