---
title: 词性
category: 学科
tags: [Alpha]
author: JQiue
article: false
---

英语是由词、句、篇构成，单词的词性是打开英语大门的钥匙，词法是英语学习的基础。词法能帮助理解许多英语概念，只有掌握了词性知识，才能理解英语句子结构，从而理解句子及整篇文章。牢固掌握每个词性的句法功能，及每个句子成分都由哪些词性的单词构成。主抓实词，兼顾虚词，总结固定词组

英语有十大词类：

词类|作用|充当成分
---|----|---|---
名词|表动作的对象|主，宾
动词|表具体动作|谓，非谓
冠词|限定名词|不能充当
代词|代替名词|主，宾
形容词|修饰名词|定，表
副词|时间、地点、程度、方式|定，状，补，表
介词|名词与其他成分之间的关系|定，状，补，表
数词|数量或顺序|主，宾，表，定
连词|连接句子成分|不能充当
叹词|表示说话感情|不能充当

有实在意义，能独立承担句子成分的是实词。没有实在意义，不能独立承担句子成分的是虚词

## 名词（Nouns）

+ 作主语：The book is on the desk.
+ 作表语：The tall man is a singer.
+ 作宾语：I found a cat on the grass.
+ 作宾语补足语：We call him a hero.
+ 作定语：I found a stone wall in the village.
+ 作同位语：Mr. Liu, our English teacher, is very handsome.
+ 作状语：The desk weighs 20 kilometers.

名词分为专有名词和普通名词，专有名词是某个人，地方，机构等专有的名称，比如 Beijing，China 等。普通名词是一类人或东西或抽象概念的名词，比如 book，sadness

普通名词又可以分为下面四类：

+ 个体名词：表示某类人或事物中的个体，如 gun
+ 集体名词：表示若干个个体组成的集合，如 family
+ 物质名词：表示无法分为个体的实物：如 air
+ 抽象名词：表示动作、状态、品质、感情等抽象概念，如 work

甚至还有种名词是有若干个单词组合而构成的复合名词，比如：girlfriend

个体名词和集体名词可用数目来计算，称为**可数名词**，物质名词和抽象名词一般无法用数目来计算，称为**不可数名词**

可数名词在形容两个或多个以上时要进行变化：

+ 一般末尾加`s`，如 bag -> bags
+ 以 s、ch、sh 结尾加`es`，如 bus -> buses
+ 以`辅音字母 + y`结尾的将`y`变为`i`，再加`es`，如 baby -> babies
+ 以 f、fe，结尾的将`f`变为`v`，再加`es`，如 knife -> knives
+ 以`o`结尾加`es`（非外来词或缩写形式），如 potato -> potatoes
+ 不规则变化

可数名词前可以加`a/an`或量词进行修饰，看情况变复数

```
an apple, a box of apples, a tomato, a bag of tomatoes
```

不可数名词前不能加`a/an`，没有复数形式，但可以用量词修饰

```
a cup of coffee
```

一些名词可以加`'s`表示所有关系，这种词尾形式的名词称为该名词的所有格，如 a teacher's book

变化规律如下：

+ 单数或复数名词词尾加`'s`
+ 若名词已有复数词尾`s`，只加`'`，如 the workers' struggle
+ 表示店铺或教堂或某人的家时，常常不出现所修饰的名词，如 the bucher's
+ 两个名词并列，分别有`'s`,表示“分别有”，如果只有一个名词有，则表示“共有”

还一种表示所有关系的形式：of 所有格，常用来表示无生命的名词，如 the windows of the room

## 冠词（Articles）

冠词是虚词，不能单独使用，也没有词义，用在名词的前面帮助说明名词的含义，有三种冠词：

+ 不定冠词
+ 定冠词
+ 零冠词

不定冠词 a/an 都是用来限定名词的数量，在意义上没有区别。a 用于辅音音素，an 用于元音音素

+ 具有不确定的意义
+ 只能用于单数可数名词
+ 表示一个，如 a Mr. Wang waiting for you
+ 表示一类人或物，如 Mr. Smith is an engineer

::: tip
不定冠词 a/an 和数词 one 同源，都表示“一个”的意思
:::

::: tip 快速区分 a/an
This is a B/C/D/G/J/K/P/Q/R/T/U/V/W/Y/Z.  
This is an A/E/F/H/I/L/M/N/O/S/X.
:::

定冠词 the 用来表示特定的人或事物，可用于单数、复数以及不可数名词连用

+ 上文中提到的人或事
+ 表示双方都明白的人或事物，如 Take the medicine
+ 世上独一无二的事物，如 the sun
+ 与单数名词连用表示一类事物，如 the dollar
+ 与复数名词连用表示整个群体，如 They are the teachers of this school
+ 与形容词或分词连用表示一类人，如 the rich
+ 用在序数词和形容词最高级前，如 I live on the second floor
+ 用在专有名词前，如 the People's Republic of China
+ 用在乐器名词前，如 the piano
+ 用在姓氏前表示一家人，如 the Greens
+ 表示所有关系，相当于物主代词，用在身体部位的名词前，如 She caught me by the arm （她抓住了我的手臂）

有些名词不需要加冠词，这些不加冠词的名词就是零冠词

+ 国家，洲，人名
+ 泛指的复数名词
+ 抽象名词和物质名词表示一般概念
+ 季节、月份、节日、假日、日期、星期等表示时间的名词
+ 称呼、官衔、职位
+ 三餐、球类运动、娱乐运动
+ 两个名词并列
+ by 与交通工具连用，表示一种方式
+ 不用冠词的序数词：序数词前有物主代词、序数词作副词、固定词组

## 动词（verbs）

系动词是用来辅助主语的动词。它本身有词义，但不能单独用作谓语

+ 状态
+ 感官
+ 变化
+ 保持
+ 表像

表示状态的只有 be 动词，就是对主语补充说明，没有意义，be 动词后面接名词、形容词、地点副词或短语作补足语

形式：be，is，am，are，was，were，being，been

+ be：将来时或虚拟语气
+ is/are：单数/复数
+ was/were：单数/复数
+ being/been：现在分词/过去分词

```
The man is back.
They are back.
He was back.
They were back.
They have been back.
Mary's new dresses are colorful.
My mother was in the kitchen.
```

在 am，is，are，was，were 后加 not 表示否定意义，也产生了缩写形式，比如 isn't，aren't 等

be 动词一般现在时在不同的人称下只有 is，am，are 变化

表示人体感官的系动词有 sound，taste，look，feel，smell等，后面接形容词

用来表示"看起来像"这一概念，主要有seem, appear, look

用来表示主语继续或保持一种状况或态度，主要有keep, remain, stay, lie, stand

这些系动词表示主语变成什么样，变化系动词主要有become, grow, turn, fall, get, go, come, run

表示主语已终止动作，主要有prove, turn out, 表达"证实"，"变成"之意

实义动词在句中具有实在意义，而不是像 be 动词一样无意义，比如：come，read，go，watch，play，fly

+ 不及物（vi.）：不需要宾语
+ 及物（vt.）：需要宾语

::: tip
动词 + 介词，通常是不及物
:::

```
He comes from Huibei.
She is reading story books.
```

::: tip 动词的第三人称单数形式
一般现在时，主语是第三人称单数的时候，动词要变成单三形式，通常加`s`
:::

::: tip
在过去式中动词没有单复数变化
:::

```
I don't go to shcool by bus.
She doesn't watch TV every day.
They didn't swim last night.
```

完全动词 have 的意思相当于“拥有”、“具有”，当 have 作为“拥有”讲时，可用于一般时态，但不能用于进行时态

```
Do you/we/they have any ...?
Yes, I/we/they have some ...?
No, I/we/they do not/don't have any ...?
```

have 常与表示疼痛和疾病的名词连用，有一下用法：

+ 必须用不定冠词，a cold、a heaache
+ 不定冠词可用可不用，catch (a) cold、have (a) backache
+ 复数形式的疾病前不用冠词

助动词没有实在意义，帮助构成时态和语态，疑问句，否定句，比如 be，do，have，will，shall

情态动词

+ can/could 表示能力，用 be able to 代替 can/could 现在过去的能力，客观可能性表示请求和允许

```
He can/could/is able to swim.
He can/could come tomorrow.
Can/Could I stay here?
```

+ may/might 表示可能性，may 的可能性较大，请求允许用 might 更委婉

```
He may/might come here by bus.
May/Might I join you?
```

+ must/have to 表示必须，必要，must 表示主观上多一些，而 have to 则表示客观多一些，have to 有时态和数量变化，must 和 have to 二者之间的否定意义有所不同

```
You must get up early.
It's going to rain, I have to go home now.
You mustn't go. 你不能去
You don't have to go. 你不必去
```

+ should/ought to 表示劝告，建议，命令，should 强调主观看法，而 ought to 强调客观要求，在疑问句中通常用 should 代替 ought to

```
You should/ought to do the job right now.
Should they stay here now?
```

+ need/don't have to

```
He need come here early.
He needn't come here early.
```

回答 must/have to 的提问句时，否定使用 needn't/don't have to 等回答方式

```
-Must I come here early tomorow?
-No, you needn't/don't have to.
```

+ had better 表示“最好做某事”，had 虽然是过去式，但不表示过去，better 后接动词原形

```
He had better eat more.
You'd better finish it right now.
```

+ would rather 表示“宁愿，宁可，最好，还是...为好”，语感上比“had better”要轻

```
You would rather deal with it now.
```

否定形式分别为：

had better not + 动词原形
would rather not + 动词原形

```
He had better not eat more.
You would rather not deal with it now.
```

构成疑问

如果是实义动词，要借用助动词来提问，不能提前，且谓语动词要用原型，无实义的动词直接将动词提前

否定形式

如果是实义动词，要借助`助动词 + not`来构成否定，无实义的动词直接加`not`

## 代词（Pronouns）

是一种代替名词的词类，大多数代词具有名词和形容词的功能，根据意义、特征以及在句中的作用分为：

+ 人称代词
+ 物主代词
+ 指示代词
+ 反身代词
+ 相互代词
+ 疑问代词|连接代词
+ 关系代词
+ 不定代词

人称代词是表示“我”，“你”，“他她它”，“我们”，“你们”，“它们”的词，具有人称、数和格的变化，在已知的情况下使用，避免重复

+ 主格代词：I，he，she，it，you，we，they

```
I am a teacher.
He is a teacher.
you are teacher
```

+ 宾格代词：me，him，her，it，you，us，them

```
I like it.
I like them.
He like me.
We like her.
They know him.
```

物主代词表示所有关系的代词，分为形容词性和名词性

形容词性可修饰名词：

+ 单：my，your，his/her/its/，one's
+ 复：our，yours，their

```
This is my book.
We love our motherland.
Those are your socks.
```

名词性可独立作为一个名词的成分:

+ 单：mine，yours，his/hers/its，one's
+ 复：ours，yours，theirs

```
The book is ours.
The apple is hers.
This computer is theirs.
```

主格|宾格|形容词性物主代词|名词性物主代词
---|---|---|---
I|me|my|mine
You|you|your|yours
He|him|his|his
She|her|her|hers
It|it|its|its
we|us|our|ours
They|them|their|theirs

当宾语和主语是同一个人时，一般需要用反身代词

+ 单：myself，yourself，herself，himself，itself
+ 复：yourselves，ourselves，themselves

```
The old lady is talking to herself.  
The old lady is talking to herself.
Please help yourself to some fish. 随便吃一些鱼吧（作宾语）
We enjoyed ourselves last night.
The thing itself is not important. 这件事它本身不重要（作同位语）
```

指示代词用于标记人或事物的代词，通常用来替代上文中出现过的名词：this（these），that（those）

```
This is his book.
Those apples were his.
```

不定代词用于替代不确定的人或事物：one，the other，some，any，something，nothing ...，不仅如此还包含 some-，any-，no- 等合成代词

```
No one knows where he is. 没有人知道他在哪
I know nothing about this person. 我对这个人一无所知
I have something to tell you. 我有一些事告诉你
```

相互代词表示相互关系，有 each other 和 one another 两组，在实际应用中没有什么区别，如 They love each other

疑问代词在句子中用来构成特殊疑问句，且疑问代词可用作连接代词，引导名词性从句

## 形容词（Adjectives）

是一种修饰名词的词，说明人、物等是什么样或看上去是怎么样的，形容词作定语时一般放在名词前：

```
the beautiful girl
a thin woman
```

the + 形容词 = 名词复数，表示一类人或事物，后面动词用复数

```
The old need more care than the young. 老年人比年轻人需要更多的关心
```

很多形容词可以回答使用 What ... like ? 这样的疑问句，比如：

```
What's Tom like?
He's very fat.
```

形容词的比较级和最高级

形容词和副词的转换的规则

+ 一般加`ly`
+ 以`y`结尾的，变`y`为`i`再加`ly`

## 副词（Adverbs）

副词可以修饰动词、形容词、其他副词以及其他结构

```
He runs fast. 修饰动词
She is very beautiful. 修饰形容词
They work very hard.
```

副词的位置：

+ 根据情况放在助动词之后，实义动词之前或之后
+ 形容词之前，其他副词之 前或之后
+ 多个助动词时，副词一般放在第一个助动词后

```
He speaks very fast.
They have already left.
They have already been repaired.
```

常用的频度副词（akways，usually，often，sometimes，never）的位置通常放在一般动词前面，be 动词后面，助动词和实意动词之间

```
They always come early.
Sam often writes homework at 7:00.
```

## 数词（Numerals）

数词分为基数词和序数词，基数词用来表示数目的多少，序数词用来表示顺序

基数词一般是单数形式，在下面情况中用复数：

+ 与 of 短语连用
+ 表示“一排”或“一组”的词组，如 They arrived in twos and threes.
+ 表示“几十岁”
+ 表示“年代”，用 in + the + 数词复数

序数词可以用来表示顺序，倍数，分数等，必须和 the 进行连用

表示数字：

```
101 one handred and one.
1,010 one thousand and ten.
1,117 one thousand one hundred and seventeen.
10,000 ten thousand
200,000 two hundred thousand
1,000,000 one milion
```

年月日表达：

+ 基本顺序是月，日，年
+ 具体时刻用 at
+ 具体某一天用 on
+ 大于一天用 in

终点表达：

+ 某个钟点的 1 到 30 分钟内，用 past 表示，比如 twenty past eight(8 点 20)
+ 过了 31，用 to 表示，比如 6 点 47 分通常会转换为 7 点差 13 分，thirteen to seven

表示分数，构成：基数词作分子，序数词作分母。当分子大于 1 时，分子用单数形式，分母用复数形式

```
1/3 one-third
3/37 three and three-sevenths
```

表示百分数，构成：基数词 + percent

表示倍数

+ 主 + 谓 + 倍数 + as + 形容词 + as ...
+ 主 + 谓 + 倍数 + the size of ...
+ 主 + 谓 + 倍数 + 形容词/副词比较级 + than + ...
+ by + 倍数

## 介词（Prepositions）

没有实在意义，按形式分类：

+ 简单介词
+ 复合介词
+ 短语介词
+ 双重介词
+ 分词介词

按意义分类：

+ 时间
+ 地点
+ 原因，目的
+ 所属，伴随
+ 除了
+ 方法，手段
+ 关于

表示时间

+ in：表示世纪、时代、年月、季节、一天中的上午中午晚上
+ on：表示星期，具体的某一天
+ at：表示时刻

表示交互

+ across：穿过平面
+ over：上面（未接触）
+ between：两者之间
+ off：离开
+ along：沿着
+ in：在...里面
+ on：在...上面
+ under：在...下面
+ into：从外入内
+ out of：从内到外

at 可表示地点：

```
at the butcher's
at home
at school
```

## 叹词（Interjections）

## 连词（Conjunctions）

## 不定量表达

使用不确定数量的表达法：some，any，most，every，all

+ some，any 都表示“一些”，some 主要用在肯定句，希望能得到肯定回答时，也可以用在疑问句中，any 主要用在否定和疑问句中

```
I'd been expecting some letters the whole morning, but there weren't any for me.
```

+ most 作形容词表示大部分的，后接复数名词

```
Most people here are from China.
```

+ every 表示“每一个，所有的”，后接单数名词

```
Every one likes the film.
```

+ all 表示“所有”，后接可数名词复数、不可数名词

```
All the cars are parked in the parking lot.
All the coffee is served on time.
```

补充：both，either，neither

+ both 表示“两者都”，可作形容词，代词或副词
+ either 表示“两者中的任意一个”
+ neigher 表示“两者都不”

```
Both his eyes were severely burned.
There are trees on either side of the street.
Neither answer is correct.
```

补充：many，much，a lot of，plenty of

+ many 修饰可数名词，表许多
+ much 修饰不可数名词，表许多
+ a lot of，lots of，plenty of 可修饰可数和不可数名词

```
many books.
much water.
a lot of/lots of/plenty of water.
```

补充：a few，few

+ a few 表肯定含义，“几个”，与可数名词连用
+ few 表否定含义，“没几个”，与可数名词连用

```
A few books are put into the box. 箱子里放了几本书
Few books are put into the box. 箱子里没有几本书
```

补充：a little，little

+ a little 表肯定含义，“一点儿”，与不可数名词连用
+ little 表否定含义，“没多少”，与不可数名词连用

```
There is a little water in the bottle. 瓶子里还有一点水
There is little water in the bottle. 瓶子里没有多少水了
```

补充：none 和 no one 的意思相同，主要作代词，翻译“一个也不，一点也不”，none 可接 of 短语，动词可用单数，也可用复数，no one 不能接短语，动词只能用单数

```
No one knows the answer. 没有人知道答案
None of us have arrived. 我们都没有到
```

## 成分

句子由不同的成分组成，而不同的词性充当不同的成分，主要有以下成分

+ 主语：句子中的主体对象
+ 谓语：主体对象发生的动作
+ 宾语：主体发生的动作作用的对象
+ 宾语补语：对句子进行补充说明
+ 主语补语（表语）：可以成为主语的补语，表示主语的状态
+ 定语：修饰主体对象
+ 状语：修饰动词，形容词以及全句，表示句中的背景
+ 同位语：和句子前面的名词可互换
