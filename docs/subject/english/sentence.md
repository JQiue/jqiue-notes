---
title: 句型
category: 学科
tags: [Alpha]
author: JQiue
article: false
---

可以毫不夸张的说，句法通，英语通，它在英语中的作用相当于一幢大楼的骨架。会分析了句子成分才能理解简单句，掌握了六大句型，才会在写作中游刃有余，避免写出中国式英语，而英语句子的理解难点就在主从复合句。养成分析句子成分的习惯，当遇到难句，长句时，注意从句子成分的角度去解决问题，不要把时间放在对知识点的死记硬背上，要注意分析语言实践

## 简单句

简单句用于说明一件事，最基本的就是“名词+动词”结构，也就是“主谓”结构。分为 6 大句型：

+ S V：主 + 谓
+ S V O：主 + 谓 + 宾
+ S V O O：主 + 谓 + 间宾 + 直宾
+ S V O C：主 + 谓 + 宾 + 宾补
+ S V P： 主 + 系 + 表
+ There be 句型

主谓结构是整个句子最基本的结构，其他的句式都是由主谓结构发展而来，主谓句式由表示陈述和被陈述关系的 2 个成分组成

```
The universe remians.
宇宙长存
```

主系表结构是较为特殊的结构，系动词是用来连接主语和表语的，没有实际意义，而表语是用来修饰主语的状态，比如性质、特点、位置

```
The food is delicions.
这个食物很美味
```

主谓宾结构是最常用的结构，表示一个对象对另一个对象所做的动作

```
He took his bag and left.
他拿着他的包离开了
```

主谓宾宾结构通常作用于两个物体身上

```
Her father bought her a dictionary.
她的爸爸给她买了一本字典
```

主谓宾宾补通常用于补充说明宾语的情况

```
We made him our monitor.
我们选他当我们的班长
```

在说明或询问人、物等的存在时即可使用 There + be 结构，There + be 结构可将重要的新信息放在句末，以示强调，此结构中的主语实际上是 be 后面的名词

```
There is a book on the bookshelf.
There are some book on the bookshelf.
```

Here + be 不是固定结构，只是一个倒装句型，不一定是 Be，还可以是其他的动词

```
Here is the bus stop.
Here comes the bus!
```

## 四种类型

+ 陈述句：肯定/否定
+ 疑问句
+ 祈使句
+ 感叹句

### 陈述句

首先要知道英语的语序和中文不一定是一样的，比如下面的句子：

```
He learns English every day.（按照语序翻译：他学英语每一天）
他每天学习英语
```

不带否定含义的陈述句都是肯定句，否定句与之相反，含有一个如 not 之类的否定词，比如：

```
This is my umbrella.
This is not my umbrella.
```

### 疑问句

通过主谓倒装可将带有 be 的陈述句变为一般疑问句，比如：

```
This is your handbag. 这是你的手提包.
Is this your handBag? 这是你的手提包吗？
```

针对一般疑问句的否定的简略答语是：No, it's not / it isn't.

当对某些具体的部分进行提问时，就要用到特殊疑问句，特殊疑问句由`特殊疑问词 + 一般疑问句`组成

特殊疑问词有：

+ what：询问是什么
+ when：询问时间
+ who：询问主语
+ how：询问方式
+ where：询问地点
+ why：询问原因
+ which：询问哪一个，哪一些
+ whose：询问所属

对`He bought three books yesterday`不同的部分进行提问

```
询问主语：Who bought three books yesterday?
询问宾语：What did he buy three books yesterday?
询问时间：When did he buy three books?
```

当对非主语部分提问时，要用 Be 动词或助动词

补充：How long，How far，How often

```
They have been in China for three years. 对时间提问
How long have they been in China?

It is about 4 kilometers from Beijing to Xi'an. 对距离提问
How far is it from Beijing to Xi'an.

They come to visit me once a week. 对频率提问
How often do they come to visit. 原句中没有助动词，要借助一下

She came late. because she missed the bus. 对原因提问
Why did she came late?
```

::: tip
How 和一些形容词结合，可以形成更丰富的提问
:::

what ... like? 通常用于询问事物的情况，比如天气或气候等，或者人物和事物的外观或特征，用形容词进行回答

```
What's the weather like today?
What's your brother like?
```

whose 用来询问所有关系，回答时总是一个名字加`'s`形式，或者是名词所有格代词

```
Whose is that car?
That is mine.
```

which 用来询问物体或物质，which 总是一种限定的，特指的选择：

```
Which book?
```

where 用来询问地点，回答可以是整句、短语或单个单词

```
Where is the book?
On the table.
```

含有 or 的疑问句称为选择疑问句，或者将选择的余地缩小在数目有限的事物内，可以有无限性的选择，这种疑问句不能简单的用 Yes 或 no 进行回答

```
Beef or lamb?
What would you like to drink?
```

### 祈使句

祈使句的主语通常省略，其谓语动词用原型，用来表示请求、建议、命令、叮嘱等

```
Follow me.
Shut the door, please.
Look out!
```

有些祈使句可以跟 and 和另一个动词，而不是带 to 的动词不定式

```
Come and meet our employees.
Wait and see.
```

否定缩写形式：Don't + 动词原形

```
Don't wait!
Don't speak to me like that!
```

### 感叹句

+ what 引导
+ how 引导

## 并列

## 从句

## 特殊句型

### 强调

### 倒装

### 虚拟
