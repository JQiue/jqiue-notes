---
title: 函数
category: 编程语言
tags: [Alpha]
author: JQiue
article: false
---

## 可变参数

也可以声明可变参数来适应不知道参数数量的情况，这样传入的参数都会保存在一个数组中

```java
void setName(String... names){}
```

可变参数无法传入`null`，因为它实际上是一个空数组

::: tip
当返回类型设置为`void`时，`return`可以省略
:::

## lambda

## 闭包

## 柯里化

<!-- to be update -->