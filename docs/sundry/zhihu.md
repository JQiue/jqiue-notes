---
title: 你的知乎和我的不一样
category: 知识分享
tags: [Beta]
time: 2019-06-22
author: 曾加
---

这里收集的是我在知乎上看到的好文，让我直呼学到了

+ [曾加老师所认为的木桶原理应该是这样的](https://www.zhihu.com/question/21466130/answer/39337466)
+ [好家伙一个CSS垂直居中就这么多花样](https://zhuanlan.zhihu.com/p/44439903)
+ [哪种IDE能同时写java和前端代码?（真修仙属于是）](https://www.zhihu.com/question/291786472/answer/526486234)
+ [怎么提高表达能力？](https://www.zhihu.com/question/19727047/answer/522905472)
+ [视频，游戏怎么定义流畅呢？](https://zhuanlan.zhihu.com/p/48674410)
+ [小小耳机也不简单](https://zhuanlan.zhihu.com/p/45744451)
+ [什么样的人学习效果最差](https://www.zhihu.com/question/305792030/answer/589663139)
+ [孙悟空到底有几个版本？](https://www.zhihu.com/question/39854920/answer/262842344)
+ [如何长时间高效学习？](https://www.zhihu.com/question/28358499/answer/73162464)
+ [如何在GitHub上做一个优秀的贡献者](https://www.zhihu.com/question/310488111/answer/585336948)
