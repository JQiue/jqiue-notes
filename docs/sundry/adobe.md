---
title: Adobe 全家桶的用法
category: 知识分享
tags: [Alpha]
author: JQiue
article: false
---

这里是 Adobe 全家桶软件的一些用法

## Adobe Photoshop

## Adobe Illustrator

+ 选择工具组
  + 选择工具：选择、移动、缩放、旋转
  + 直接选择工具：选择、移动、变形
  + 套索工具：自由选择锚点
  + 魔棒工具：色容差
+ 填色和描边
  + 前后置换：x
  + 颜色置换：Shift + x
  + 默认黑白：D
  + 关闭：/
+ 描边
  + 可改颜色
  + 实现、虚线
  + 端点
  + 折角

<!-- more -->