---
title: 如何使用 Babel 放飞自我
category: Web
tags: [Alpha]
author: JQiue
article: false
---

Babel 是一个 JavaScript 转义器，它用于将使用了最新的 ECMAScript 特性代码转换成了使用最广泛支持的 ES5 特性的等价代码，让开发人员享受新特性带来的爽点同时，避免了大部分兼容性问题

<!-- more -->
