---
title: 计算机原理
author: JQiue
article: false
---

踏入计算机领域的第一步

## 目录

- [计算机组成原理](./organization/basic)
- [编译原理](./compile/basic)
- 操作系统原理
- [计算机网络原理](./network/basic)
- 数据库系统原理
