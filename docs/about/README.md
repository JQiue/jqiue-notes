---
title: 关于 - 本站
tags: [必读]
author: JQiue
sticky: true
---

::: info 感谢观看
本站增加了 PWA 支持，拥有离线缓存的能力，这意味着下一次无需联网即可访问，也可以将本站作为 APP 安装在本地，拥有接近原生 APP 般的使用体验。当本站已经更新且你没有清除本站的缓存时，再次访问将从右下角看到更新内容的按钮

如果你有幸访问到这里，应该做好没有收获的准备！
:::


## 这个人的信息

![照骗（长太丑浏览器并不想显示）](/)

+ 互联网 ID：JQiue
+ QQ：861947542
+ 邮箱：jqiue@foxmail.com
+ Github：[https://github.com/JQiue](https://github.com/JQiue)
+ 目前使用的站点域名：[jinqiu.wang](https://jinqiu.wang)，~~[wjqis.me](https://wjqis.me)~~

## 他会点儿什么

只会用 C，Java，JavaScript，Python 写写 HelloWorld 的水平

会点 Web 前端开发，我 f*** 这该死的 CSS

只会一点点后端开发，使用 NodeJS，或者 Java

对于 MySQL，MongoDB 的使用仅限于增删改查的水平

基本熟练操作系统的安装与卸载，比如 Windows 和 Linux，以及黑苹果，会用那是另说

玩转各种搜素引擎，嗯，很喜欢 COPY，真的不想再总结一遍，圣经就是圣经

熟悉几种代理工具的玩法，也就上上 Google，浏览 Youtube 的水平，不会英语，即使翻出去也是摸瞎

多亏了白嫖的斐讯 k2p，玩耍 k2p 的过程中，对嵌入式设备有了一丢丢认知，k2p 已经扔到家里做路由，早就忘完了是什么鬼

也就这样子了，看起来花里胡哨的，其实是真的很菜很菜

## 他的日常

骑车、跑步、跳绳、听音乐、以及折腾各种东西

## 正在做却没做完的事

+ 整理站点笔记
+ 学习算法
+ 为 CET-4 背单词啦（~~英语永远的痛~~）
+ 练练书法，拯救我这人神共愤的字体

## 排版

本站统一中文文案，排版的相关用法，降低访问者的沟通成本，并约束所有的文档

+ 空格
  + 中英文之间添加空格
  + 中文和数字之间添加空格
  + 数字和单位之间不添加空格
  + 全角标点和其他字符之间不加空格
+ 全角和半角
  + 使用全角中文标点
  + 数字使用半角字符
  + 完整的英文整句、特殊名词、在其内容中使用半角

## 内容

本站记录的是我自大学以来学习的各类知识，想写（抄）的很多，且体系庞大，总结（抄袭）是一种很花精力的事情，无法避免有错误的地方。目前大饼画完了 20% 不到，目标是 2022 年 6 月份完成整张饼的 40%。暂时缺乏大量图片，无法避免阅读疲劳，这些将在完成度达到 50% 时补充

- [x] 10%（2021.3.21）
- [x] 20%（2021.10.28）
- [ ] 30%

所有的文章都遵循三个阶段：`Alpha`、`Beta`、`Gamma`，它们都会以标签的形式标记

+ 处于`Alpha`阶段的文章，可能啥也没有，内容仅作参考，问题较多，随时会进行修改
+ 处于`Beta`阶段的文章，内容逐渐完善，问题较少，可能退回 `Alpha`，会出现在首页文章列表
+ 处于`Gamma`阶段的文章，内容逻辑顺畅，可以阅读，会出现在首页文章列表中，可能有少许并不影响阅读的 typo

如果没有必要，不建议阅读`Alpha`阶段的文章

## 适合什么样的人阅读

+ `具有悟性的初学者`：系统性的知识体系能够快速理解内容
+ `所见即所得`：部分知识在本站上有效果演示
+ `想上进的`：对自身有意义就够了
+ `我这样的人`：给我自己看的，防止忘记

## 全站知识体系总览

> 星星表示需要完成或学习的优先级、重要程度

+ [计算机基础](/theory/)
  + 计算机组成原理:star::star:
  + 操作系统原理:star::star:
  + 编译原理:star:
  + 计算机网络:star::star::star:
  + 数据库原理:star:
+ 编程语言
  + [C](/c/):star::star:
  + [Java](/java/):star::star:
  + [JavaScript](/js/):star::star::star::star:
    + [TypeScript](/sundry/typescript/):star:
  + [Python](/python/):star:
+ [数据结构与算法](/ds-algorithm/)
  + 数据结构:star::star::star::star:
  + 排序和查找:star::star::star:
  + 算法思想:star::star:
  + 领域算法:star:
+ [数据库](/database/)
  + SQL 语言:star::star:
  + MySQL:star::star:
  + MongoDB:star::star:
  + Redis:star:
+ Web
  + [HTML-CSS](/html-css/):star::star::star::star:
  + [WebAPI](/webapi/):star::star::star::star:
+ 应用开发
  + [移动应用](/mobile/):star:
  + [后端](/backend/):star::star:
  + [桌面端](/desktop/):star:
+ 应用框架
  + [Vue](/vue/):star::star::star:
  + Spring:star::star:
  + Spring Boot:star::star:
+ 服务器 && 容器
  + [Tomcat](/sundry/tomcat/):star:
  + Apache:star:
  + [Nginx](/sundry/nginx/):star::star:
+ 其他杂项
  + 编辑器/IDE:star:
  + [Markdown](/sundry/markdown/):star:
  + CI/CD:star:
  + [Git](/sundry/git/):star::star:
  + [Window](/os/windows/) & [Linux](/os/linux/):star::star:
  + [设计模式](/sundry/design-pattern/):star::star::star:
  + Docker:star:
  + 代码质量:star::star:
  + [正则表达式](/sundry/regex/):star::star:

> 全站预估为 202500 字，可花 396 分钟浏览完成，本人日产 1000 字左右

## 错误更正

如果发现文章有误，可以通过本人联系方式通知我纠正，或者点击错误页最下方的`编辑此页`前往 Github 提交一个更正后的 PR，我将审核通过后合并

> 来都来了，留个言再走？
